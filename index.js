var http;
if (process.argv[2]) {
  // Load HTTPS module if a key is provided
  var fs = require('fs');
  http = require('https').Server({pfx: fs.readFileSync(process.argv[2])});
} else {
  // Or just load HTTP module if no key is provided
  http = require('http').Server();
}

var io = require('socket.io')(http);

io.on('connection', function(socket) {
  console.log(socket.id, 'connected');

  socket.on('join', function(room) {
    console.log(socket.id, 'joined', room);
    // Tell the other peers that someone joined the room
    io.to(room).emit('joined', socket.id);
    socket.join(room);
  });

  socket.on('signal', function(data) {
    // Retransmit signal data between peers
    io.to(data.dest).emit('signal', {
      source: socket.id,
      signal: data.signal,
    });
  });
});

http.listen(9876, function() {
  console.log('listening on *:9876');
});
